import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import configureStore from '../store';

import App from '../App';
test('renders', () => {
  const { getByText } = render(
    <Provider store={configureStore()}>
      <App />
    </Provider>,
  );
  const linkElement = getByText(/hackernews/i);
  expect(linkElement).toBeInTheDocument();
});
