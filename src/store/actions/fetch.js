import axios from 'axios';

import ACTION_TYPES from '../../constants/actionTypes';

const baseURL = 'https://hacker-news.firebaseio.com/v0/';

export const onInitLoad = () => async dispatch => {
  try {
    dispatch({
      type: ACTION_TYPES.STORIES.GET_LOAD,
    });
    const response = await axios.get(`${baseURL}topstories.json`);
    if (!response) {
      throw new Error('Error');
    }
    const data = await response.data;
    const collection = data
      .slice(0, 10)
      .map(id =>
        axios.get(`${baseURL}item/${id}.json`).then(response => response.data),
      );
    const result = await Promise.all(collection);
    dispatch({
      type: ACTION_TYPES.STORIES.GET_RES,
      payload: {
        data: result,
      },
    });
  } catch (err) {
    dispatch({
      type: ACTION_TYPES.STORIES.GET_ERR,
      error: err,
    });
  }
};

// export const fetchComments = comments => {
//   return {
//     type: STORIES,
//     comments: comments,
//   };
// };

// export const initComments = kids => async dispatch => {
//   try {
//     const promises = kids.map(kid =>
//       axios.get(`item/${kid}.json`).then(response => response.data),
//     );
//     const result = await Promise.all(promises);
//     dispatch(fetchComments(result));
//   } catch (err) {
//     dispatch(fetchDataFailed(err));
//   }
// };

// export const fetchPage = data => {
//   return {
//     type: ACTION_TYPES.STORIES.GET_RES,
//     data: data,
//   };
// };

export const onInitNextPage = pageNum => async dispatch => {
  try {
    dispatch({
      type: ACTION_TYPES.STORIES.GET_LOAD,
    });
    const response = await axios.get(`${baseURL}topstories.json`);
    const data = await response.data;
    const collection = data
      .slice(0, parseInt(pageNum))
      .map(id =>
        axios.get(`${baseURL}item/${id}.json`).then(response => response.data),
      );
    const result = await Promise.all(collection);
    dispatch({
      type: ACTION_TYPES.STORIES.GET_RES,
      payload: {
        data: result,
      },
    });
  } catch (err) {
    dispatch({
      type: ACTION_TYPES.STORIES.GET_ERR,
      error: err,
    });
  }
};
