import { combineReducers } from 'redux';

/** Data state */
// A
// B
// C
// D
import storeStory from './story';
import storeStories from './stories';
// G
// H
// M
// S
// T
// U
// Z

const rootReducer = combineReducers({
  // A
  // B
  // C
  // D
  storeStory,
  storeStories,
  // G
  // H
  // M
  // S
  // T
  // U
});

export default rootReducer;
