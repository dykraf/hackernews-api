import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

/* library */
import {
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
} from 'reactstrap';
import InfiniteScroll from 'react-infinite-scroller';
import NewsSkeleton from './components/cards/NewsSkeleton';

/* styles */
import './App.css';

/* components */
import Layout from './components/layouts/Layout';

/* cards components */
import News from './components/cards/News';

/** constants and helpers */
import { random } from './helpers/utils';

/** contexts */
import { Search } from './context/Search';

import * as action from './store/actions/index';

function App(props) {
  const { storeStories, dispatch } = props;

  const [textSearch, setTextSearch] = useState('');
  const [searchMode, setSearchMode] = useState(false);
  const [storiesCollection, setStoriesCollection] = useState([]);

  // const [isLoading, setIsLoading] = useState(true);
  const [isLoading] = useState(true);
  const [modal, setModal] = useState(false);
  const [modalContent, setModalContent] = useState(false);

  const [nextPage, setNextPage] = useState(20);

  useEffect(() => {
    if (dispatch) {
      dispatch(action.onInitLoad());
    }
  }, [dispatch]);

  useEffect(() => {
    if (textSearch === '') {
      setSearchMode(false);
    }
  }, [textSearch]);

  useEffect(() => {
    if (storeStories.status === 'success') {
      setTimeout(() => {
        setStoriesCollection(storeStories.data);
      }, 3000);
    }
  }, [storeStories]);

  function onNextPageHandler() {
    // set nextPage number
    setNextPage(nextPage + 10);
    // get next page
    setTimeout(() => {
      dispatch(action.onInitNextPage(nextPage));
    }, 1000);
    setStoriesCollection([...storeStories.data, ...storiesCollection]);
  }

  function onChangeTextSearch(e) {
    e.preventDefault();
    setSearchMode(true);
    setTextSearch(e.target.value);
    setStoriesCollection([
      ...storeStories.data.filter(story => {
        return story.title.toLowerCase().includes(textSearch.toLowerCase());
      }),
    ]);
  }

  function toggleModal(propsModal) {
    setModal(!modal);
    setModalContent(propsModal);
  }

  function renderModal(propsModal) {
    const { buttonLabel, url, title, author } = propsModal;
    return (
      <div>
        <Modal
          isOpen={modal}
          toggle={toggleModal}
          className="modal modal-lg"
          centered
        >
          <ModalHeader toggle={toggleModal} className="mt-2">
            {title}
          </ModalHeader>
          <ModalBody>
            <a href={url} target="_blank" rel="noopener noreferrer">
              <h4>{title}</h4>
            </a>
            <p>by: {author}</p>
            <Button
              size="sm"
              color="primary"
              onClick={toggleModal}
              className="capitalize"
            >
              <i className="fas fa-folder mr-2"></i>
              {buttonLabel}
            </Button>
          </ModalBody>
          <ModalFooter>
            <Button size="sm" color="secondary" onClick={toggleModal}>
              Close
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }

  return (
    <Search.Provider value={[textSearch, setTextSearch]}>
      <div className="App">
        <Layout onChangeTextSearch={onChangeTextSearch}>
          <InfiniteScroll
            initialLoad={true}
            loadMore={onNextPageHandler}
            hasMore={
              storeStories.data.length === 0 || searchMode ? false : true
            }
            useWindow={false}
            loader={<NewsSkeleton key={random()} />}
            threshold={240}
          >
            <Container fluid>
              <News
                isLoading={isLoading}
                storeStories={storeStories}
                storiesCollection={storiesCollection}
                onNextPageHandler={e => onNextPageHandler(e)}
                toggleModal={toggleModal}
              />
            </Container>
          </InfiniteScroll>
          {modal && renderModal(modalContent)}
        </Layout>
      </div>
    </Search.Provider>
  );
}

const mapStateToProps = state => {
  return {
    storeStories: state.storeStories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
