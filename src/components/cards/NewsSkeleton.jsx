import React from 'react';
import Skeleton from 'react-loading-skeleton';

const NewsSkeleton = () => {
  return (
    <div className="card-news" key={0}>
      <div
        style={{ display: 'flex', alignItems: 'center', alignSelf: 'stretch' }}
      >
        <div style={{ marginBottom: 10, width: '50%', textAlign: 'right' }}>
          <div style={{ marginBottom: 5, marginRight: 25 }}>
            <Skeleton width="100%" height={10} duration={0.5} />
          </div>
          <div style={{ marginBottom: 5, marginRight: 25 }}>
            <Skeleton width="75%" height={10} duration={0.5} />
          </div>
        </div>
        <div style={{ marginBottom: 0, width: '50%' }}>
          <Skeleton rounded width="100%" height={115} duration={0.5} />
        </div>
      </div>
    </div>
  );
};

export default NewsSkeleton;
