import React from 'react';
import { Row, Col, Card, CardBody, Button } from 'reactstrap';
import formatDistance from 'date-fns/formatDistance';

/* utils */
import { getHostName, random } from '../../helpers/utils';

function News(props) {
  /* stories collection */
  const { storiesCollection, /* onNextPageHandler, */ toggleModal } = props;
  return (
    storiesCollection.length > 0 &&
    storiesCollection.map(story => {
      return (
        <Card key={random(story.id - 1000)} className="card-news p-0">
          <CardBody>
            <Row noGutters={false}>
              <Col xs="6" className="mb-3 text-left">
                <p>
                  <a href={story.url || '#'}>{story.title}</a>
                  {story.url && (
                    <span
                      className="text-secondary"
                      style={{
                        marginLeft: '.4rem',
                        fontWeight: 'bold',
                      }}
                    >
                      ({getHostName(story.url)})
                    </span>
                  )}
                </p>
                <ul
                  style={{
                    fontSize: '.78rem',
                    width: '100%',
                    listStyle: 'none',
                  }}
                  className="d-inline small p-0 m-0 text-secondary"
                >
                  <li className="d-inline-block capitalize mr-2">
                    <i className="fas fa-star mr-1"></i>
                    {story.score} Points {/* by {story.by} */}
                  </li>
                  <li className="d-inline-block capitalize mr-2">
                    <i className="fas fa-clock mr-1"></i>
                    {story.time &&
                      formatDistance(new Date(story.time * 1000), new Date(), {
                        includeSeconds: true,
                      })}
                  </li>
                  <li className="d-inline-block capitalize mr-2">
                    <i className="fas fa-folder mr-1"></i>
                    {story.type}
                  </li>
                  <li className="d-inline-block capitalize mr-2">
                    <i className="fas fa-comment-alt mr-1"></i>
                    {story.descendants} Comments
                  </li>
                </ul>
              </Col>
              <Col xs="6" className="mb-3">
                <div className="card-avatar">
                  <h3 className="text-secondary">by: {story.by}</h3>
                </div>
              </Col>
              <Col
                xs={{ size: 12, order: 3 }}
                lg="12 mx-auto text-center"
                style={{
                  position: 'absolute',
                  bottom: '-1rem',
                  zIndex: '999',
                }}
              >
                <Button
                  color="primary"
                  className="icon-rounded-circle"
                  onClick={e =>
                    toggleModal({
                      buttonLabel: story.type,
                      url: story.url,
                      title: story.title,
                      author: story.by,
                    })
                  }
                  value={story.id}
                >
                  <span className="text-light font-weight-bold">+</span>
                </Button>
              </Col>
            </Row>
          </CardBody>
        </Card>
      );
    })
  );
}

export default News;
