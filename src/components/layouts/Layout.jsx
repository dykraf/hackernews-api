import React from 'react';
import { connect } from 'react-redux';

/* components */
import Header from '../header/Header';
import Footer from '../footer/Footer';

function Layout(props) {
  const { children, /* isScrolled, */ onChangeTextSearch } = props;
  return (
    <>
      <Header onChangeTextSearch={onChangeTextSearch} />
      <div className="App-body">{children}</div>
      <Footer />
    </>
  );
}

export default connect(state => state)(Layout);
