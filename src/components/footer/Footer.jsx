import React from 'react';
import { NavbarBrand } from 'reactstrap';

function Footer(props) {
  /* const { children } = props; */
  return (
    <footer className="App-footer bg-primary" id="App-Footer">
      <h6>
        <NavbarBrand
          className="App-link-logo logo text-light"
          href="https://news.ycombinator.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
          HackerNews.
        </NavbarBrand>
      </h6>
      {/* {children} */}
    </footer>
  );
}

export default Footer;
