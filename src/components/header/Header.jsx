import React, { useState, useContext } from 'react';
import {
  Collapse,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

/** contexts */
import { Search } from '../../context/Search';

function Header(props) {
  const { onChangeTextSearch } = props;
  const [textSearch] = useContext(Search);
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <>
      <header className="App-header">
        <Navbar color="light" light expand="xxl">
          <NavbarBrand
            className="App-link-logo logo theme-color"
            href="https://news.ycombinator.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Hacker<span className="text-dark">News</span>
            <span className="theme-color">.</span>
          </NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav navbar className="ml-auto">
              <NavItem>
                <NavLink href="https://news.ycombinator.com/newest">
                  News
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://news.ycombinator.com/front">
                  Past
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://news.ycombinator.com/newcomments">
                  Comments
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://news.ycombinator.com/ask">Ask</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://news.ycombinator.com/show">Show</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://news.ycombinator.com/jobs">Jobs</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://news.ycombinator.com/submit">
                  Submit
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </header>
      <div className="App-search bg-primary" id="App-Search">
        <form>
          <div className="form-group container">
            <label htmlFor="query" className="mb-0 pb-0 col-12">
              <span className="sr-only">Search</span>
              <div className="input-group mb-2">
                <input
                  type="text"
                  className="form-control box-shadow-0 border-bottom border-light rounded-0 text-light font-weight-bold"
                  id="inlineFormInputGroup"
                  placeholder=""
                  value={textSearch}
                  onChange={e => onChangeTextSearch(e)}
                  style={{
                    backgroundColor: 'transparent',
                    borderTop: '0',
                    borderLeft: '0',
                    borderRight: '0',
                  }}
                />
                <div
                  className="input-group-append border-0"
                  style={{ backgroundColor: 'transparent', border: 0 }}
                >
                  <div
                    className="input-group-text"
                    style={{
                      backgroundColor: 'transparent',
                      border: 0,
                      fontSize: '1.25rem',
                    }}
                  >
                    <i className="fas fa-search text-light"></i>
                  </div>
                </div>
              </div>
            </label>
          </div>
        </form>
      </div>
    </>
  );
}

export default Header;
