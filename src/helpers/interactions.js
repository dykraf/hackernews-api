/* window scroll detect */
export function windowScroll({ isScrolled, update }) {
  const { scrollY } = window;

  if (scrollY < 150 && isScrolled !== false) {
    update(false);
  }

  if (scrollY >= 150 && isScrolled !== true) {
    update(true);
  }
}
