/* params to string */
export function paramsToString(
  params = {},
  excludeEmptyValue = true,
  sort = true,
) {
  const paramsArray = [];

  Object.entries(params).forEach(param => {
    if (excludeEmptyValue) {
      if (param[1]) {
        paramsArray.push(`${param[0]}=${encodeURIComponent(param[1])}`);
      }
    } else {
      paramsArray.push(`${param[0]}=${encodeURIComponent(param[1])}`);
    }
  });

  if (sort) paramsArray.sort();

  return paramsArray.length ? `?${paramsArray.join('&')}` : '';
}

/* get hostname */
export function getHostName(url) {
  var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
  if (
    match != null &&
    match.length > 2 &&
    typeof match[2] === 'string' &&
    match[2].length > 0
  ) {
    return match[2];
  } else {
    return null;
  }
}

/* random number */
export function random(num) {
  return Math.floor(Math.random() * num || 100);
}
