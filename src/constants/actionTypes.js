/** Redux action types */
// A
// B
// C
// D
// E
// F
// G
// H
// ...
// P
// Q
// R
// S
const STORY = {
  GET: 'story_get',
  GET_LOAD: 'story_get_load',
  GET_RES: 'story_get_result',
  GET_ERR: 'story_get_error',
};
const STORIES = {
  GET: 'stories_get',
  GET_LOAD: 'stories_get_load',
  GET_RES: 'stories_get_result',
  GET_ERR: 'stories_get_error',
};
// T
// U
// ...

export default {
  // A
  // B
  // C
  // D
  // E
  // F
  // G
  // H
  // ...
  // M
  // ...
  // P
  // Q
  // R
  // S
  STORY,
  STORIES,
  // ...
  // U
  // T
  // ...
};
