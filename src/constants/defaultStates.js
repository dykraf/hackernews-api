/** Store's default state */
import STATUS_TYPES from './statusTypes';

// A
// B
// D
// E
// F
// G
// H
// ...
// M
// ...
// P
// Q
// R
// S
const STORY = {
  status: STATUS_TYPES.INIT,
  error: null,
  data: [],
};
const STORIES = {
  status: STATUS_TYPES.INIT,
  error: null,
  data: [],
  filters: [],
  pagination: {},
};
// T
// U
// ...

export default {
  // A
  // B
  // C
  // D
  // E
  // F
  // G
  // H
  // ...
  // M
  // ...
  // P
  // Q,
  // R
  // S
  STORY,
  STORIES,
  // T
  // U
  // ...
};
