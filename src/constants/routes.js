const STORY = {
  // D
  STORY: {
    href: '/story/story',
    route: '/story',
    url: '/story',
  },
  STORIES: {
    href: '/stories/stories',
    route: '/stories/:id',
    url: '/stories',
  },
};

module.exports = {
  ...STORY,
};
