/* Fetch's endpoint */
export default {
  // A
  // ...
  // B
  // C
  COMMENTS: '/item',
  // D
  STORY: '/item',
  STORIES: '/topstories',
  // E
  // ...
  // H
  // ...
  // L
  // M
  // M
  // ...
  // P
  // R
  // ...
  // S
  // T
  // U
  // ...
};
