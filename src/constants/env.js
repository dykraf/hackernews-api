const ENV = process.env.NODE_ENV;

const ENV_TYPES = {
  LOCAL: 'local',
  DEV: 'dev',
  SANITY: 'sanity',
  PRODUCTION: 'production',
};

const API_TYPES = {
  LOCAL: 'https://hacker-news.firebaseio.com/v0',
  DEV: 'https://hacker-news.firebaseio.com/v0',
  STAGING: 'https://hacker-news.firebaseio.com/v0',
  PRODUCTION: 'https://hacker-news.firebaseio.com/v0',
};

export default {
  ENV_TYPES,
  API_TYPES,
  ENV,
};
